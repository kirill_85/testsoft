import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import {ProjectService} from "../../service/project.service";
import {stringify} from "@angular/compiler/src/util";

@Component({
  selector: 'app-load-data',
  templateUrl: './load-data.component.html',
  styleUrls: ['./load-data.component.css']
})
export class LoadDataComponent implements OnInit {
  lbl = new FormControl({
  value: 'Введите JSON'
});
  inputArea = new FormControl('', [Validators.min(3), Validators.required]);
  loadButton = new FormControl();
  loadForm: FormGroup = this.fb.group(new FormGroup({
    lbl: this.lbl,
    inputArea: this.inputArea,
    loadButton: this.loadButton
  })
  );
  constructor(private router: Router,
              private fb: FormBuilder,
              private service: ProjectService) { }

  ngOnInit(): void {

  }

  loadData(content: string): void {
    if (content != null) {
      this.service.create(content);
    }
    this.router.navigateByUrl('/projects').then((result: boolean) => localStorage.setItem('log', stringify(result)));
  }
}
