import { Component, OnInit } from '@angular/core';
import {ProjectModel} from "../../models/project-model";
import {ProjectService} from "../../service/project.service";

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  currentProject: ProjectModel = {
    cost: 0, createdBy: "", description: "", endDate: "", id: 0, startDate: "", subject: ""

  };

  constructor(private service: ProjectService) { }

  ngOnInit(): void {

  }

  update(): void {

  }
}
