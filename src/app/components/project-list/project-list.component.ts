import { Component, OnInit } from '@angular/core';
import {ProjectService} from "../../service/project.service";
import {ProjectModel} from "../../models/project-model";

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  projectList: ProjectModel[];
  displayedColumns: string[];

  constructor(private service: ProjectService) {
    this.projectList = [];
    this.displayedColumns = ['col-subject', 'col-createdBy', 'col-startDate', 'col-endDate'];
  }

  ngOnInit(): void {
    this.projectList = this.service.getProjects();
  }

}
