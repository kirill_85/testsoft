import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {ProjectModel} from "../../models/project-model";
import {FormBuilder, FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.css']
})
export class ProjectCardComponent implements OnInit {
  @Input() project: ProjectModel;
  @Input() disabled: boolean;
  @Output() onUpdateProject = new EventEmitter<ProjectModel>();
  startDate = new FormControl({}, [Validators.required]);
  endDate = new FormControl({}, [Validators.required]);
  description = new FormControl({}, [Validators.required])
  formGroup = this.fb.group({
    startDate: this.startDate,
    endDate: this.endDate,
    description: this.description
  });

  constructor(private fb: FormBuilder) {
    this.project = {
      cost: 0, createdBy: "", description: "", endDate: "", startDate: "", subject: "",
      id: -1
    };
    this.disabled = false;
  }

  ngOnInit(): void {

  }

  updateProject(): void {
    this.onUpdateProject.emit({
      cost: this.project.cost,
      createdBy: this.project.createdBy,
      description: this.project.description,
      endDate: this.project.endDate,
      id: this.project.id,
      startDate: this.project.startDate,
      subject: this.project.subject
    });
  }

}
