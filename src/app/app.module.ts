import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectDetailComponent } from './components/project-detail/project-detail.component';
import {AppRoutingModule} from "./app-routing.module";
import {ProjectService} from "./service/project.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadDataComponent } from './components/load-data/load-data.component';
import {ReactiveFormsModule} from "@angular/forms";
import { ProjectCardComponent } from './components/project-card/project-card.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectListComponent,
    ProjectDetailComponent,
    LoadDataComponent,
    ProjectCardComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
    ],
  providers: [ProjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
