import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { Route } from  '@angular/router';
import {ProjectListComponent} from "./components/project-list/project-list.component";
import {ProjectDetailComponent} from "./components/project-detail/project-detail.component";
import {LoadDataComponent} from "./components/load-data/load-data.component";

const routes: Route[] = [
  {path: '', pathMatch: 'full', redirectTo: 'load'},
  {path: 'load', component: LoadDataComponent},
  {path: 'projects', component: ProjectListComponent},
  {path: 'projects/:id', component: ProjectDetailComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
