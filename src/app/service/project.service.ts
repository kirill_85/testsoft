import { Injectable } from '@angular/core';
import {ProjectModel} from "../models/project-model";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private readonly LOCAL_DATA_ID = 'projects';

  private _projects: ProjectModel[];

  constructor() {
    this._projects = [];
  }

  getProject(id: number): ProjectModel {
      const data = this.projects.find(item => item.id === id) as ProjectModel;
      return data;
  }

  getProjects(): ProjectModel[] {
    const data = localStorage.getItem(this.LOCAL_DATA_ID);
    if (data != undefined) {
      const jsonData: ProjectModel[] = <ProjectModel[]>(JSON.parse(data)["Projects"]);

      for (let item of jsonData) {
        let project: ProjectModel = {
          id: item.id,
          subject: item.subject,
          description: item.description,
          createdBy: item.createdBy,
          startDate: item.startDate,
          endDate: item.endDate,
          cost: item.cost
        };
        this._projects.push(project);
      }
    }

    return this._projects;
  }

  create(data: string): void {
    localStorage.setItem(this.LOCAL_DATA_ID, data);
  }

  public get projects(): ProjectModel[] {
    return this._projects;
  }
}
